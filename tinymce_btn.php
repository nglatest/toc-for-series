<?php
/**
 * User: Sukhdeep Singh (http://techsmashing.com)
 */
/*
Copyright 2015  Sukhdeep Singh  (email : techsyard@gmail.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
new ToC_TinyMce_Btn();
class ToC_TinyMce_Btn {
    public function __construct()
    {
        add_action('admin_head', array($this, 'tscom_shortcode_button'));
     }

    public function tscom_shortcode_button()
    {
        // check user permissions
        global $typenow;

        // only on Post Type: post and page
        if( ! in_array( $typenow, array( 'post', 'page' ) ) )
            return;

        add_filter( 'mce_external_plugins', array($this,'tscom_add_buttons') );
        add_filter( 'mce_buttons', array($this,'tscom_register_buttons') );
    }

   public function tscom_add_buttons( $plugin_array )
    {
        $plugin_array['tscom_toc'] = plugins_url('/js/tinymce_btn.js',__FILE__);
        return $plugin_array;
    }

     public function tscom_register_buttons( $buttons )
    {
        array_push( $buttons, '|', 'tscom_toc' );
        return $buttons;
    }

}