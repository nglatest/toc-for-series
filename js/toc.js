jQuery(document).ready(function () {
    var $toc_select = jQuery('#toc-post-select'), $toc_text = jQuery('#toc-post-text');
    $toc_select.change(function () {
        if ($toc_select.val() == 'other') {
            $toc_text.removeAttr('disabled');
        } else {
            $toc_text.attr('disabled', 'disabled').val('');
        }
    }).trigger('change');

    //form submission
    jQuery(function() {

        jQuery("#toc_defaults").click(function(e) {

            var id = jQuery('.nav-tab-active').attr('id');
            //default values are passed through 'pass_default_to_js' fuunction in main.php
            if (id == 'display'){
                //reset the options with default_opts variable received from
                jQuery('#toc_google_api').val(default_opts.google_fonts_api_key);
                jQuery('#toc_title_text').val(default_opts.toc_title);
                jQuery('#toc_title_font_size').val(default_opts.toc_title_font_size);
                jQuery('#toc_title_font_face').val(default_opts.toc_title_font_face);
                jQuery('#toc_title_font_style').val(default_opts.toc_title_font_style);
                //jQuery('#toc_title_font_url').val(default_opts.toc_title_font_url);
               jQuery('#toc-list-font-size').val(default_opts.toc_list_font_size);
                jQuery('#toc_list_font_face').val(default_opts.toc_list_font_face);
                jQuery('#toc_list_font_style').val(default_opts.toc_list_font_style);
                //jQuery('#toc_list_font_url').val(default_opts.toc_list_font_url);
                 jQuery('#toc_custom_css').val(default_opts.toc_custom_css);
            }else if (id == 'colors')
            {
                jQuery('#toc_title_font_color').val(default_col_opts.toc_title_font_color);
                jQuery('#toc_title_bg_color').val(default_col_opts.toc_title_bg_color);
                jQuery('#toc_title_border_color').val(default_col_opts.toc_title_border_color);
                jQuery('#toc_list_font_color').val(default_col_opts.toc_list_font_color);
                jQuery('#toc_list_bg_color').val(default_col_opts.toc_list_bg_color);
                jQuery('#toc_list_hover_bg_color').val(default_col_opts.toc_list_hover_bg_color);
                jQuery('#toc_list_hover_fg_color').val(default_col_opts.toc_list_hover_fg_color);
                jQuery('#toc_list_cur_post_fg_color').val(default_col_opts.toc_list_cur_post_fg_color);
                jQuery('#toc_list_cur_post_bg_color').val(default_col_opts.toc_list_cur_post_bg_color);
            }
            jQuery("#toc-form").submit();

        });

    });

    // Add Color Picker to all inputs that have 'color-field' class
    var wp_cp_options = {
        // you can declare a default color here,
        // or in the data-default-color attribute on the input
        defaultColor: false,
        // a callback to fire whenever the color changes to a valid color
        change: function(event, ui){},
        // a callback to fire when the input is emptied or an invalid color
        clear: function() {},
        // hide the color picker controls on load
        hide: true,
        // show a group of common colors beneath the square
        // or, supply an array of colors to customize further
        palettes: true
    };


    jQuery(function() {
        var cp_options =  {
            layout:'rgbhex',
            submit:0,
            onChange:function(hsb,hex,rgb,el,bySetColor) {
                jQuery(el).css('background-color','#'+hex);
                // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                if(!bySetColor) jQuery(el).val('#'+hex);
            }
        };
        jQuery('.color-field').colpick(cp_options).click(function(){
            jQuery(this).colpickSetColor(this.value);
        }).keyup(function(){
            jQuery(this).colpickSetColor(this.value);
        });;

        //jQuery('.color').colorPicker();
        //jQuery('.color-field').wpColorPicker(wp_cp_options);
    });

    /*This function will be called whenever value will be changed inside font-face selectors/dropdowns.
    * On calling ,it shall populate the next dropdown with the respective style like bold, italic etc.
    * It shall also set the font-url inside the hidden field*/
    function event_on_font_face_change(thisObj, saved_font, saved_style, $fstyle_dropdown, $font_url_id)
    {
	$font_face_id = thisObj.val();
        $gfonts_obj = gfonts_atts.gfonts[$font_face_id];
        var obj = $gfonts_obj["font_weights"];
 	
        $fstyle_dropdown.html('');
        jQuery.each( obj, function( index, val ) {
            var sel_txt = '';
            if(saved_font == $font_face_id && saved_style == val)
                sel_txt = ' selected';
            $fstyle_dropdown.append('<option value="' + val + '"' + sel_txt +'>' + val + '</option>');
        });

        var st_val= $fstyle_dropdown.val();
        $font_url_id.val($gfonts_obj["urls"][st_val]);
	
    }
    /*This function will be called whenever value will be changed inside font-style selectors/dropdowns.
    * On calling ,it shall populate the font-url inside the hidden field*/
    function event_on_font_style_change(thisObj, $font_face_id, $font_url_id)
    {
	$fface_id = $font_face_id.val();
        $gfonts_obj = gfonts_atts.gfonts[$fface_id];

        var st_val= thisObj.val();
        $font_url_id.val($gfonts_obj["urls"][st_val]);
	
    }
    
     /*Set dropdowns for toc-title's fonts*/
    var font_title_face_dropdown=jQuery('#toc_title_font_face');
    var font_title_style_dropdown=jQuery('#toc_title_font_style');
    var font_title_url_lbl=jQuery('#toc_title_font_url');
    
    font_title_face_dropdown.change(function() {
        var saved_font = gfonts_atts.title_saved_font;
        var saved_style = gfonts_atts.title_saved_style;
	event_on_font_face_change(jQuery(this), saved_font, saved_style, font_title_style_dropdown, font_title_url_lbl)
    }).trigger('change');

    font_title_style_dropdown.change(function() {
	    event_on_font_style_change(jQuery(this), font_title_face_dropdown, font_title_url_lbl);
    }).trigger('change');

    /*Set dropdowns for toc-list's fonts*/
    var font_face_dropdown=jQuery('#toc_list_font_face');
    var font_style_dropdown=jQuery('#toc_list_font_style');
    var font_url_lbl=jQuery('#toc_list_font_url');
    font_face_dropdown.change(function() {
        var saved_font = gfonts_atts.saved_font;
        var saved_style = gfonts_atts.saved_style;
	event_on_font_face_change(jQuery(this), saved_font, saved_style, font_style_dropdown, font_url_lbl)
 
     }).trigger('change');

    font_style_dropdown.change(function() {
	event_on_font_style_change(jQuery(this), font_face_dropdown, font_url_lbl);
    }).trigger('change');

    //accordions on help tab
    function close_accordion_section() {
        jQuery('.accordion .accordion-section-title').removeClass('active');
        jQuery('.accordion .accordion-section-content').slideUp(300).removeClass('open');
    }

    jQuery('.accordion-section-title').click(function(e) {
        // Grab current anchor value
        var currentAttrValue = jQuery(this).attr('href');

        if(jQuery(e.target).is('.active')) {
            close_accordion_section();
        }else {
            close_accordion_section();

            // Add active class to section title
            jQuery(this).addClass('active');
            // Open up the hidden content panel
            jQuery('.accordion ' + currentAttrValue).slideDown(300).addClass('open');
        }

        e.preventDefault();
    });

    // Open up the hidden content panel
    jQuery('.accordion #accordion-1').slideDown(0).addClass('open');
});