/**
 * Created by Sukhdeep Singh (http://techsmashing.com) on 4/9/2015.
 */
(function () {
    tinymce.PluginManager.add('tscom_toc', function(editor, url) {
        // Add a button that opens a window
        editor.addButton('tscom_toc', {
            title: 'TSCom ToC Shortcode',
            image: url + '/../img/Cone.png',
            onclick: function() {
                // Open window
                editor.windowManager.open({
                    title: 'TSCom ToC Shortcode Properties',
                    body: [
                        { type: 'textbox', name: 'title', label: 'Title'},
                        { type: 'label', name: 'title_label', style: 'height: 30px',
                            multiline: true,
                            onPostRender : function() {
                                this.getEl().innerHTML =
                                    "specify the title of the list. leave blank for no title.<br />"+
                                    " "
                            }
                        },
                        { type: 'listbox', name: 'hdr_depth', label: 'Header Depth',
                            'values': [
                                {text: 'h1', value: 'h1'},
                                {text: 'h2', value: 'h2'},
                                {text: 'h3', value: 'h3'}
                            ]
                        },
                        { type: 'listbox', name: 'series', label: 'Series',
                            'values': [
                                {text: 'test1', value: 'test1'},
                                {text: 'none', value: 'none'},
                            ]
                        }
                    ],
                    onsubmit: function(e) {
                        // Insert content when the window form is submitted
                        editor.insertContent('[tscom-toc \
									hdr_depth="' + e.data.hdr_depth + '" \
									series="' + e.data.series + '" \
                        ]' + e.data.title + '[/tscom-toc]');
                    }
                });
            }
        });

        // Adds a menu item to the tools menu
        editor.addMenuItem('tscom_toc', {
            text: 'TSCom ToC Help',
            context: 'tools',
            onclick: function() {
                // Open window with a specific url
                editor.windowManager.open({
                    title: 'TSCom ToC site',
                    url: 'http://techsmashing.com',
                    width: 800,
                    height: 600,
                    buttons: [{
                        text: 'Close',
                        onclick: 'close'
                    }]
                });
            }
        });
    });

})();