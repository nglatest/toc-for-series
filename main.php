<?php
/*
Plugin Name: TOC for Article Series
Description: Display all related articles and their respective headers as a tree inside the contents
Author: Sukhdeep Singh
Author URI: http://techsmashing.com
Version: 1.0
*/
/*
Copyright 2015  Sukhdeep Singh  (email : techsyard@gmail.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as 
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
defined( 'ABSPATH' ) or die( 'TOC AS Plugin file cannot be accessed directly.' );

/*ini_set('display_errors', 'On');
error_reporting(E_ALL);*/

define('TOT_TOC_HT', 28);
define("TSCOM_TOC_MAIN_STYLE_FILE", "toc-style.css");
define("TSCOM_TOC_GEN_STYLE_FILE", plugin_dir_path( __FILE__)."css/toc-gen-style.css");
define ("TOC_LOCAL_GFONTS_FILE", plugin_dir_path(__FILE__) . 'gfonts.json');
define("GOOGLE_FONT_URL", "https://www.googleapis.com/webfonts/v1/webfonts?sort=popularity&key=");
//define("GOOGLE_FONT_URL", "https://www.googleapis.com/webfonts/v1/webfonts?key=");

include_once	plugin_dir_path( __FILE__)."htmls/admin-settings-form.php";
include_once 	plugin_dir_path( __FILE__).'tinymce_btn.php';

add_action('admin_menu', 'toc_plugin_setup_menu');
add_action( 'admin_head', 'toc_enqueue_ie7_hook' );

/*add_filter( 'pre_update_option_toc_custom_css', 'option_toc_custom_css' );
function option_toc_custom_css( $newvalue ) {
	return 180;
}*/

if( !is_admin() ) {
	add_action( 'wp_enqueue_scripts', 'toc_enqueued_assets' );
	add_action('wp_head','toc_enqueue_ie7_hook');
}

function toc_enqueued_assets() {
	$toptions = get_option('toc_options');
	if (preg_match("/(?<=family=)\S+/", $toptions['toc_list_font_url'], $matches)) {
		$str = $toptions['toc_title_font_url']."|".$matches[0];
	}
	//if google fonts are not uploaded then upload them at very first
	if ( !wp_style_is( 'toc-fonts', 'enqueued' ) ) {
		wp_register_style('toc-fonts', $str);
		wp_enqueue_style('toc-fonts');
	}
	 // Enqueue the styles in they are not already...
	if ( !wp_style_is( 'toc-style', 'enqueued' ) ) {
		wp_register_style( 'toc-style', plugin_dir_url( __FILE__ ) . 'css/toc-gen-style.css' );
		wp_enqueue_style('toc-style');

		wp_register_style('font-awesome', plugin_dir_url( __FILE__ ) . 'font-awesome/css/font-awesome.min.css');
		wp_enqueue_style('font-awesome');

		wp_register_style( 'toc-jqtree', plugin_dir_url( __FILE__ ) . 'css/jquery.treemenu.css',array("toc-fonts") );
		wp_enqueue_style('toc-jqtree');

	}

	if ( !wp_script_is( 'toc-jqtree-js', 'enqueued' ) ) {
		//<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
		wp_enqueue_script('jquery');
		wp_register_script('toc-jqtree-js', plugin_dir_url( __FILE__ ) . 'js/jquery.treemenu.js' , array("jquery"), '1.0', true);
		wp_enqueue_script('toc-jqtree-js');
		wp_register_script('tscom-toc-js', plugin_dir_url( __FILE__ ) . 'js/tscom-toc-js.js' , array("jquery", "toc-jqtree-js"), '1.0', true);
		wp_enqueue_script('tscom-toc-js');

	}

}

function toc_enqueue_ie7_hook()
{
	$output = '';
	$output .= "<!--[if IE 7]>\n <link rel='stylesheet' href='".plugin_dir_url( __FILE__ ) . "font-awesome/css/font-awesome-ie7.min.css'>\n<![endif]-->\n";
	echo $output;
}
function admin_enqueued_assets() {

	if ( !wp_style_is( 'toc-admin', 'enqueued' ) ) {
		wp_register_style('toc-title', '//fonts.googleapis.com/css?family=Tangerine');
		wp_enqueue_style('toc-title');

		wp_register_style( 'toc-admin', plugin_dir_url( __FILE__ ) . 'css/admin.treemenu.css');
		wp_enqueue_style('toc-admin');
		//wp_enqueue_style( 'wp-color-picker' );
		wp_register_style('font-awesome-admin', plugin_dir_url( __FILE__ ) . 'font-awesome/css/font-awesome.min.css');
		wp_enqueue_style('font-awesome-admin');
		wp_register_style('toc-cp', plugin_dir_url( __FILE__ ) . 'thirdparty/colpick/css/colpick.css');
		wp_enqueue_style('toc-cp');

	}

	if ( !wp_script_is( 'toc', 'enqueued' ) ) {
		//<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
		wp_enqueue_script('jquery');
	    wp_register_script('toc', plugin_dir_url( __FILE__ ) . 'js/toc.js' , array('jquery','wp-color-picker'), '1.0', true);
	    wp_enqueue_script('toc');
	/*	wp_register_script('toc-cp', plugin_dir_url( __FILE__ ) . 'thirdparty/tinyColorPicker/jqColorPicker.min.js' , array('jquery'), '1.0', true);
		wp_enqueue_script('toc-cp');*/
		wp_register_script('toc-cp', plugin_dir_url( __FILE__ ) . 'thirdparty/colpick/js/colpick.js' , array('jquery'), '1.0', true);
		wp_enqueue_script('toc-cp');

	}
} 

function toc_plugin_setup_menu(){
    add_menu_page( 'toc Plugin Page', 'TSCom ToC', 'manage_options', 'toc-plugin', 'toc_init','dashicons-list-view' );
    add_action( 'admin_enqueue_scripts', 'admin_enqueued_assets' );
	add_action( 'admin_init', 'toc_plugin_settings' );

}

function toc_plugin_settings() {
	register_setting( 'toc-plugin-settings-group', 'toc_options', 'opt_validation' );
	register_setting( 'toc-plugin-colors-group', 'toc_col_options' );
}

function pass_default_to_js()
{
	    // DEFAULT OPTIONS ARRAY
	$def_opts = array(
	'google_fonts_api_key' => '',
	'toc_title' => 'Table of Articles',

	'toc_title_font_size' => '16',
	'toc_title_font_face' => 'roboto',
	'toc_title_font_style' => '700',
	'toc_title_font_url' =>  '',

	'toc_list_font_size' => '12',
	'toc_list_font_face' => 'raleway',
	'toc_list_font_style' => '300',
	'toc_list_font_url' => '',

	'toc_custom_css' => '');

	$def_col_opts = array(
		'toc_title_bg_color' => '#34495E',
		'toc_title_font_color' => '#ffffff',
		'toc_title_border_color' => '#34495E',

		'toc_list_bg_color' => '#2C3E50',
		'toc_list_font_color' => '#46CFB0',
		'toc_list_hover_bg_color' => '#34BC9D',
		'toc_list_hover_fg_color' => '#fff',

		'toc_list_cur_post_bg_color' => '#34495E',
		'toc_list_cur_post_fg_color' => '#fff'
	);
    wp_localize_script('toc', 'default_opts', $def_opts);
	wp_localize_script('toc', 'default_col_opts', $def_col_opts);
}

function toc_init(){
    if ( !current_user_can( 'manage_options' ) )  {
        wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }
    pass_default_to_js();
    
    $toptions = get_option('toc_options');
	$tcoptions = get_option('toc_col_options');

	$google_fonts_api_key = $toptions['google_fonts_api_key'];
	$gfonts = get_google_fonts($google_fonts_api_key);

	display_toc_settings_form($gfonts, $toptions, $tcoptions	);
	
	modify_tscom_toc_style($gfonts, $toptions, $tcoptions);
	
}

function opt_validation	($input)
{
    $new_input = $input;

    if( isset( $input['google_fonts_api_key'] ) )
        $new_input['google_fonts_api_key'] = sanitize_text_field( $input['google_fonts_api_key'] );
	
	if(!preg_match('/^[a-z0-9]/i', $new_input['google_fonts_api_key'])) {
		$new_input['google_fonts_api_key'] = '';
	}

    if( isset( $input['toc-title-font-size'] ) )
        $new_input['toc-title-font-size'] = absint( $input['toc-title-font-size'] );

    if( isset( $input['toc_title'] ) )
        $new_input['toc_title'] = sanitize_text_field( trim($input['toc_title']) );
	
	if(!preg_match('/^[a-z0-9]/i', $new_input['toc_title'])) {
		$new_input['toc_title'] = '';
	}
	   
	return $new_input;

}

function colors_validation($input){
	return $input;
}

function get_google_fonts($google_fonts_api_key)
{

	$google_fonts_json = '';
	$google_fonts = array();
	$gfonts = '';
	$retVal = false;
	$read_from_local=false;
	//if stored in transisent cache then retrieve that, else fetch it.
	//delete_transient('tscom_google_fonts_list');
	if ( ($gfonts =  get_transient( 'tscom_google_fonts_list' )) !== false ) {
		return $gfonts;
	}
	else {
		$google_fonts_json = dnld_google_fonts($google_fonts_api_key);

		//if could not downloaded from google, then try to read from local
		if (!$google_fonts_json) {
			if(DIRECTORY_SEPARATOR == '\\') //windows
				$google_fonts_json = file_get_contents(TOC_LOCAL_GFONTS_FILE);
			else
				$google_fonts_json = wp_remote_fopen(TOC_LOCAL_GFONTS_FILE);

			$read_from_local = true;
		}

		//if google_fonts list is still not retrieved then fetch it from some repository
		if (!$google_fonts_json) {
			$google_fonts_json = wp_remote_fopen("https://github.com/toc-for-series/webfonts.json", array('sslverify' => false));
		}

		if ($google_fonts_json) {
			$google_fonts = json_decode($google_fonts_json, true);
		}

		foreach ( $google_fonts['items'] as $item ) {

			$urls = array();

			// Get font properties from json array.
			foreach ( $item['variants'] as $variant ) {

				$name = str_replace( ' ', '+', $item['family'] );
				$urls[ $variant ] = "https://fonts.googleapis.com/css?family={$name}:{$variant}";
				$styles[$variant] = find_font_style($variant);

			}

			$atts = array(
				'name'         => $item['family'],
				'font_type'    => 'google',
				'category'		=> $item['category'],
				'font_weights' => $item['variants'],
				'styles'       => $styles,
				'subsets'      => $item['subsets'],
				'files'        => $item['files'],
				'urls'         => $urls
			);

			// Add this font to the fonts array
			$id           = strtolower( str_replace( ' ', '_', $item['family'] ) );
			$gfonts[ $id ] = $atts;

		}
		// Set transient for google fonts
		set_transient( 'tscom_google_fonts_list', $gfonts, 7 * DAY_IN_SECONDS );
		file_put_contents(plugin_dir_path( __FILE__ ) .'gfonts_modified.json',
			json_encode($gfonts, JSON_PRETTY_PRINT));

		//if file was either downloaded from google or some repo then store it
		//locally, otherwise leave it.
		if(!$read_from_local)
			file_put_contents(TOC_LOCAL_GFONTS_FILE,$google_fonts_json);
	}
	//var_dump($gfonts);
	return $gfonts;
}

function dnld_google_fonts($google_fonts_api_key){
	$fonts_json = null;

	if(function_exists('wp_remote_get')){
		$gf_url = GOOGLE_FONT_URL.$google_fonts_api_key;
		$retval = wp_remote_get($gf_url, array('sslverify' => false));

		if( !is_wp_error($retval)){
			if(isset($retval['body']) && $retval['body']){
				//Even if it contains some content but verify that it's not some error msg
				if(strpos($retval['body'], 'error') === false){ //no error
					$fonts_json = $retval['body'];
				}else{
					$error = json_decode($retval['body']);
					$gf_messages[] = '<span class="slight">Google API Notice: '. $error->error->code. ": ". $error->error->message . '</span>';

				}
			}
		}else{

			$gf_messages[] = "Unable to connect to Google's Webfont server at <a href='".$gf_url."' target='_blank'>".$gf_url."</a>.";
			foreach($retval->errors as $error) {
				foreach ($error as $message) {
					$gf_messages[] = $message;
				}
			}
		}
	}
	return $fonts_json;
}

function modify_tscom_toc_style($gfonts, $toptions, $tcoptions)
{
$file 	= TSCOM_TOC_GEN_STYLE_FILE;

$font_path = plugin_dir_url( __FILE__ ) . 'css/fonts/';

	$tface = $toptions['toc_title_font_face'];
	$lface = $toptions['toc_list_font_face'];

	$tf_type = $gfonts[$tface]['font_type'];
	$lf_type = $gfonts[$lface]['font_type'];

	$title_font_face_family = "'".$gfonts[$tface]['name']."', '".$gfonts[$tface]['category']."'";
	$list_font_face_family =  "'".$gfonts[$lface]['name']."', '".$gfonts[$lface]['category']."'";

$str 	.= '@import url('.TSCOM_TOC_MAIN_STYLE_FILE.');'."\n";
// Write the contents to the file,
// using the FILE_APPEND flag to append the content to the end of the file
// and the LOCK_EX flag to prevent anyone else writing to the file at the same time
file_put_contents($file, $str,  LOCK_EX);
$styles = "@font-face {\n";
$styles = $styles ."font-family: 'raleway';\n";
$styles = $styles ."src: url('".$font_path."raleway-regular-webfont.eot');\n";
$styles = $styles ."src: url('".$font_path."raleway-regular-webfont.eot?#iefix') format('embedded-opentype'),\n";
$styles = $styles ."url('".$font_path."raleway-regular-webfont.woff') format('woff'),\n";
$styles = $styles ."url('".$font_path."raleway-regular-webfont.ttf') format('truetype'),\n";
$styles = $styles ."url('".$font_path."raleway-regular-webfont.svg#ralewayregular') format('svg');\n";
$styles = $styles ."font-weight: normal;\n";
$styles = $styles ."font-style: normal;\n";
$styles = $styles ."}\n";


$styles .= "#tscom-tree { background-color:".$tcoptions['toc_list_bg_color'].";}\n";
$styles .= "#tscom-tree .tree li a {
color:".$tcoptions['toc_list_font_color'].";
font-family: ".$list_font_face_family.", 'raleway', Arial;
font-size:".$toptions['toc-list-font-size']."px;
} \n";

if($lf_type == 'google')
{
	$styles .= "#tscom-tree .tree li a {\n";
	$ret = find_font_style($toptions['toc_list_font_style']);
	if(isset($ret['weight']) ) $styles .= "font-weight:".$ret['weight'].";\n";
	if(isset($ret['style']) ) $styles .= "font-style:".$ret['style'].";\n";
	$styles .= "} \n";
}

$styles .= "#tscom-tree .tree li a:hover,
.cur_post:hover {
background-color: ".$tcoptions['toc_list_hover_bg_color'].";
color: ".$tcoptions['toc_list_hover_fg_color'].";
}\n";

$styles .= ".cur_post,
.active {
background-color: ".$tcoptions['toc_list_cur_post_bg_color'].";
color: ".$tcoptions['toc_list_cur_post_fg_color'].";
}\n";

$styles .= ".tree-empty > span.toggler:before {  color:".$tcoptions['toc_list_font_color']."; }
.tree-closed > span.toggler:before { color:".$tcoptions['toc_list_font_color'].";  }
.tree-opened > span.toggler:before {  color:".$tcoptions['toc_list_font_color']."; }\n";

//Next are for title bar styles.
$styles .= "#tscom-tree-title{
border-bottom:1px solid ".$tcoptions['toc_title_border_color'].";
background-color: ".$tcoptions['toc_title_bg_color'].";
color: ".$tcoptions['toc_title_font_color'].";
font-size: ".$toptions['toc_title_font_size']."px;
font-family: ".$title_font_face_family.", 'raleway', Arial;
}\n";

if($tf_type == 'google')
{
	$styles .= "#tscom-tree-title {\n";
	$ret = find_font_style($toptions['toc_title_font_style']);
	if(isset($ret['weight']) ) $styles .= "font-weight:".$ret['weight'].";\n";
	if(isset($ret['style']) ) $styles .= "font-style:".$ret['style'].";\n";
	$styles .= "} \n";
}
$styles .= $toptions['toc_custom_css']."\n";
	file_put_contents($file, $styles, FILE_APPEND | LOCK_EX);

}

function find_font_style($input)
{
	$ret = array();
	$fstyle = preg_split("/(?<=\d)(?=[a-z])/", $input);
	if(is_numeric($fstyle[0])){
		$ret['weight'] = trim($fstyle[0]);
		//if it's something like 700italic then store that also
		//otherwise ignore that part.
		if($fstyle[1] == 'italic'){  
			$ret['style'] = trim($fstyle[1]);
		}
	}
	else if($fstyle[0] == 'regular')
	{
		$ret['style'] = 'normal';
	}
	else{
		$ret['style'] = trim($fstyle[0]);
	}
	return $ret;
}

/******************************************************************
 *Part 2: Sidebar option in post editing screen.
 *Two options available right now.
 *1: unique anchor id generation option for each header
 *2: selection of series name if needed
******************************************************************/
/*Display post option to specify tag for series on post-edit screen*/
add_action( 'load-post.php', 'toc_post_meta_boxes_setup' );
add_action( 'load-post-new.php', 'toc_post_meta_boxes_setup' );


/* Meta box setup function. */
function toc_post_meta_boxes_setup() {

  /* Add meta boxes on the 'add_meta_boxes' hook. */
  add_action( 'add_meta_boxes', 'toc_add_post_meta_boxes' );
    /* Save post meta on the 'save_post' hook. */
  add_action( 'save_post', 'toc_save_post_class_meta', 10, 2 );
}

function toc_add_post_meta_boxes() {

  add_meta_box(
    'toc-post-class',
    __( 'TSCom Series ToC Option', 'toc-Option' ),
    'toc_post_class_meta_box',
    'post',        
    'side',       
    'default'
  );
}

function toc_post_class_meta_box( $post, $box ) {

	// Add an nonce field so we can check for it later.
	wp_nonce_field( basename( __FILE__ ), 'toc_series_class_nonce' );
	$content_post = get_post($post->ID);
	$content 	= $content_post->post_content;
	$uniq_anch 	= 'yes';
	
	/* Use get_post_meta() to retrieve an existing value
	* from the database and use the value for the form.*/
	$value = esc_attr(get_post_meta( $post->ID, 'tscom_toc_series_key', true ));
	$uniq_anch = esc_attr(get_post_meta( $post->ID, 'tscom_toc_uniq_anch_key', true ));
    
    $other_select="selected";
?>

	<table class="toc-form-table" title="Select various options for this article">
        <tr valign="bottom">
        <th scope="row" title="Ensure anchors are always given unique name">Unique anchor</th>
		<td><input type="checkbox" title="Ensure anchors are always given unique name" name="toc_uniq_anch" id="toc_uniq_anch" value="yes" <?php if ($uniq_anch=="yes") echo 'checked="checked"'; ?>/></td>
        </tr>
		
		<tr valign="bottom">
        <th scope="row" title="If want to be part of any existing series. Otherwise select 'other'">Series</th>
        <td><select id="toc-post-select" name="toc-post-select" title="If want to be part of any existing series. Otherwise select 'other'">
<?php
	//Get list of all meta keys of series
	$pos = get_unique_post_meta_values('tscom_toc_series_key');
	if($pos){
		foreach( $pos as $position){
    		if ($value == $position){
				$other_select = "";
 				echo '<option value="'. $position . '" selected>'.$position.'</option>';
    		}
 			else
	   			echo '<option value="'. $position . '">'.$position.'</option>';
		}
	}
	
	echo '<option value="other" '.$other_select.'>other</option>';
?>	
		</select></td>
        </tr>
	
		<tr valign="bottom">
        <th scope="row" title="If want to create a new series, then first select other from above and then enter new name here">New Series</th>
        <td><input type="text" name="toc-post-text" title="If want to create a new series, then first select other from above and then enter new name here" id="toc-post-text" size=10 value="" /></td>
        </tr>
	</table>
<?php 	
}

/* Save the meta box's post metadata. */
function toc_save_post_class_meta($post_id, $post) {

	/* Verify the nonce before proceeding. */
	if (!isset($_POST['toc_series_class_nonce']) || !wp_verify_nonce($_POST['toc_series_class_nonce'], basename(__FILE__)))
		return $post_id;

	// Check the user's permissions.
	if (isset($_POST['post_type']) && 'page' == $_POST['post_type']) {
		if (!current_user_can('edit_page', $post_id)) {
			return $post_id;
		}
	} else {

		if (!current_user_can('edit_post', $post_id)) {
			return $post_id;
		}
	}

	/* Get the posted data and sanitize it for use as an HTML class. */
	$new_meta_value = (isset($_POST['toc-post-select']) ? sanitize_text_field($_POST['toc-post-select']) : '');

	if ($new_meta_value == "other")
		$new_meta_value = (isset($_POST['toc-post-text']) ? sanitize_text_field($_POST['toc-post-text']) : '');

	//update in database
	update_toc_meta('tscom_toc_series_key', $new_meta_value, $post_id);

	//Update unique anchor generation option
	$uniq_anch_meta_value = (isset($_POST['toc_uniq_anch']) ? sanitize_text_field($_POST['toc_uniq_anch']) : '');

	update_toc_meta('tscom_toc_uniq_anch_key', $uniq_anch_meta_value, $post_id);
	return;
}

function update_toc_meta($meta_key, $new_meta_value, $post_id) {
	/* Get the meta value of the custom field key. */
	$meta_value = get_post_meta($post_id, $meta_key, true);

	/* If a new meta value was added and there was no previous value, add it. */
	if ($new_meta_value && '' == $meta_value)
		add_post_meta($post_id, $meta_key, $new_meta_value, true);

	/* If the new meta value does not match the old value, update it. */
	elseif($new_meta_value && $new_meta_value != $meta_value)
	update_post_meta($post_id, $meta_key, $new_meta_value);

	/* If there is no new meta value but an old value exists, delete it. */
	elseif('' == $new_meta_value && $meta_value)
	delete_post_meta($post_id, $meta_key, $meta_value);
}

function get_unique_post_meta_values( $key = '', $type = 'post', $status = 'publish' ) {

	global $wpdb;

	if( empty( $key ) )
		return;

	$res = $wpdb->get_col( $wpdb->prepare( "
		SELECT DISTINCT pm.meta_value FROM {$wpdb->postmeta} pm
		LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
		WHERE pm.meta_key = '%s'
		AND p.post_status = '%s'
		AND p.post_type = '%s'
		", $key, $status, $type ) );
	
		return $res;
}

/******************************************************************
 *Part 3: Updating the post in database.
 * 1) If post is selected as part of series then anchor ids for each
 * header will be gnerated before saving the post in database.
 * 2) If user has selected unique-anchor generation option then a
 * unique number will be appended to each anchor
 * 3) Also, anchor ids will be generated as seperate class instead of
 * embedding them in header itself to avoid any user-provided anchor
******************************************************************/

//add_action( 'wp_insert_post_data', 'tscom_add_anchors_if_series', 99, 2  );

add_filter( 'publish_post', 'tscom_add_anchors_if_series' , 10, 3 );
remove_filter("the_contnet", "wpautop");
//add_filter("publish_post", "shortcode_unautop", 13);

function tscom_add_anchors_if_series($post_id, $post)
{
	$uniq_anch = esc_attr(get_post_meta( $post_id, 'tscom_toc_uniq_anch_key', true ));
    
	// Get the posted data and sanitize it for use as an HTML class. 
	$new_meta_value = ( isset( $_POST['toc-post-select'] ) ?  sanitize_text_field( $_POST['toc-post-select'] ) : '' );
  
	if ($new_meta_value == "other")
	  $new_meta_value = ( isset( $_POST['toc-post-text'] ) ?  sanitize_text_field( $_POST['toc-post-text'] ) : '' );
	  
	//if this neta value is still null then it means modification is not requested for this post
	if ($new_meta_value == '')
		return;

	//Get post content which we will be modifying before saving to database
	$content = get_post($post_id)->post_content;
	$post_type = get_post_type($post);
	
    if ( trim($content) == '' ) {
        return ;
    }

    $content_lines = explode("\n", $content);

    if ( count($content_lines) == 0 ) {
        return ;
    }
	
	if($post_type != 'post')
		return ;
	
	//find for:  *<span class="tscom-ctree-anchor" id="what_is_a_hook?" ></span>
	// if required anchors were previously found then we need to update/remove them
	// as per the new settings and content.
	//$new_content = get_tscom_tag("id", $content, "span", "class=\"tscom-ctree-anchor");
	
	$count 		= 	0;
	$class 		= 	"tscom-ctree-anchor";
	$hdr_regex 	= 	preg_quote("#(.*?)<h[1-6][^>]*>(.*?)</h[1-6]>#s");
	$span_regex	=	"#span.*class\s*=\s*['\"](.*?)['\"][^>]*>(.*?)<\/span>#s";
	$id_regex 	= 	"#<span.*id\s*=\s*['\"](.*?)['\"][^>]*>(.*?)<\/span>#s";
	
	$lines = array();
	foreach($content_lines as $line) {
		if (preg_match('/(.*?)\<h[1-6][^>]*\>(.*)<\/h[1-6]\>/', $line, $matches, PREG_OFFSET_CAPTURE)) 
		{
			//$debug = 'here1';
			$span_text_match = array();
			$link = '';
			$hdr_text = '';
			$offset = $matches[2][1];
			$txt_len_to_replace = 0;
			$class_val = $class;
			
			//First delete any previous anchors created.But capture header text from there
			if (preg_match($span_regex, $line, $span_text_match)) {
				$class_found = $span_text_match[1];
				
				//if class found contains toc-class string also then nothing to
				//append. otherwise append founded class val to toc-class string
				$class_val = (strpos($class_found, $class) === false)?
					$class_val = $class.' '.$class_found : $class_found;

				//find anchor-id 
				$span_id_match = '';
				if (preg_match($id_regex, $line, $span_id_match)) 
				{
					$link = $span_id_match[1];
				}

				$txt_len_to_replace = strlen($span_text_match[0])+1;
				$hdr_text = $span_text_match[2];

			}
			else
			{
				//1st position would be any extra characters before header tag
				$hdr_text = trim ( preg_replace($hdr_regex, "$2", strip_tags($line)) );
				$txt_len_to_replace = strlen($hdr_text);
			}

			//if no previos anchor-id is there, then generate a new-one
			if($link == '')
			{
				$link = str_replace(" ", "_", strtolower($hdr_text) );
				
				//if uniqueness is also requested then add rand-number
				$unq_val = '';
				if ($uniq_anch == 'yes')
				{
					$link = $link."_".wp_rand(100, 999); 
				}
			}
			
			//Also removing any new line from the text string
			$anchor_line = "<span class=\"".$class_val."\" id=\"".$link."\">".$hdr_text."</span>";
			
			//$matches[2][1] will give the position in the header tag where to insert our anchor line.
			$changed_line = substr_replace($line, $anchor_line, $offset, $txt_len_to_replace );
			
			$lines[] = $debug.($changed_line); //( preg_replace($hdr_regex, $changed_line, strip_tags($line)) );
			
			$count++;
		} else {
			$lines[] = ($line);
		}
	}
	
	//if no header link was updated then simply returns
	if (0 == $count)
		return;
	
	//$rslt = "Total ".$i." headers updated.".$extra;
	
	$new_content = implode("\n", $lines);

	// unhook this function so it doesn't loop infinitely
	remove_action( 'publish_post', 'tscom_add_anchors_if_series', 10, 3 );
	
	// update the post, which calls save_post again
	wp_update_post( array( 'ID' => $post_id, 'post_content' => $new_content) );
	
	// re-hook this function
	add_action( 'publish_post', 'tscom_add_anchors_if_series', 10, 3 );
	return;
}

/*To find value of a string:
 *<span class="tscom-ctree-anchor" id="what_is_a_hook?" ></span>
 *use $tag : span
 *$helper:  class="tscom-ctree-anchor
 *$attr: id
 *$value: ? [1]
 */
function get_html_attr_value($attr, $html, $tag = null, $helper = null) {
	if (is_null($tag))
		$tag = '\w+';
	else
		$tag = preg_quote($tag);

	$attr = preg_quote($attr);
	$helper = preg_quote($helper);

	$input_line = ($html);
	//$tag_regex =  '/<('.$tag.')\\s*('.$helper.')[^>]*'.$attr.'\s*=\s*([\'"])(.*?)\3[^>]*>(.*?)<\/\\1>/';
	//https://regex101.com/r/bZ5fT1/1, http://www.phpliveregex.com/, https://www.functions-online.com/preg_match.html
	$tag_regex = "/<".$tag."\s*".$helper."[^>]*".$attr."\s*=\s*\"(.*?)\"[^>]*>(.*?)<\/".$tag.">/";
	
	preg_match_all($tag_regex, $input_line, $output_array, PREG_PATTERN_ORDER);
	$error = preg_last_error();

	return ($output_array);
}

/******************************************************************
 *Part 4: Shortcode implementation for ToC
 *To display ToC if shortcode is found in the post
 *****************************************************************/
function register_tscom_shortcodes(){
	add_shortcode('tscom-toc', 'tscom_toc_content_display');
}

/*HOOK INTO WORDPRESS*/
add_action( 'init', 'register_tscom_shortcodes');

/* By default, wordpress does not allow shortcodes in sidebar widgets.
But this can be enable with single line of code. */
add_filter('widget_text', 'do_shortcode');
function tscom_toc_content_display($atts, $title = null)
{
	if (is_search() || is_archive() || is_feed() || is_front_page())
		return;

	extract(shortcode_atts(array(
		'width' => 100,
		'height' => 100,
		'series' => '',
		), $atts));
	
	$series = trim($series);
	if ($series == '') {
		/* Use get_post_meta() to retrieve current post's meta value*/
		$series = esc_attr(get_post_meta(get_the_ID(), 'tscom_toc_series_key', true));

		//if values is still null then it means no series is intended for it.
		if ($series == '')
			return;
	}
	remove_filter("the_content", "do_shortcode", 11);
	$content = apply_filters("the_content", get_the_content());
	add_filter("the_content", "do_shortcode", 11);
	$content = strip_tags($content, "<h1>");
	$content = explode("\n", $content);
	$counter = count($content);
	
	$args = array(
		'meta_key' => 'tscom_toc_series_key',
		'meta_value' => $series,
		'orderby' => 'date',
		'order' => 'ASC',
		'post_type' => 'post',
		'posts_per_page' => -1,
		'post_status' => 'publish');
	$query = new WP_Query($args);

	if ( ($found_posts = $query -> found_posts) == 0)
		return;
	global $post;
	$cur_postId =  $post->ID;
	$tree='';
	//If title is not supplied under shortcode then fetch from db
	if (trim($title) == '' ) {
		$toptions = get_option('toc_options');
		$title = $toptions['toc_title'];
	}
	$tree .= '<div id="tscom-tree" class="tscom-toc-container"><div id="tscom-tree-title">'.$title."</div>\n".'<ul class="tree tscom-hdr-1">'."\n";

	while ($query -> have_posts()) {
		$query -> the_post();
		global $post;
		$cur_post_class="";

		$title = get_the_title();
		$content = get_the_content();
		$article_link = get_permalink();
		$id = get_the_ID();

		if($id == $cur_postId)
			$cur_post_class = ' class="cur_post" ';

		$tree .=  '<li><a href="'.$article_link.'"'. $cur_post_class. ' title="'.$title.'">'.$title.'</a>';

		$tree .= get_all_hdrs_in_article( $article_link, $content );
		$tree .=  '</li>'."\n";
	}
	$tree .=  '</ul>'."\n".'</div>';
	return $tree;
}

function get_all_hdrs_in_article($article_link, $content)
{
	if ( trim($content) == '' ) {
        return ;
    }

    $content_lines = explode("\n", $content);

    if ( count($content_lines) == 0 ) {
        return ;
    }
	
	if ( false ==  preg_match_all('/(.*?)\<h([1-6])[^>]*\>(.*)<\/h[1-6]\>/', $content, $matches, PREG_PATTERN_ORDER) )
	{
		return;
	}
	
	/* Output will be something like:
	array(
	0 = >
		array(
		0 = > ' <h3 id="test">What Is A Hook?</h3>',
		1 = > '  <h4>HOW TO USE HOOKS ON YOUR WORDPRESS BLOG</h4>',
		2 = > ' <h2 id="test">No Hook?</h2>', ),
	1 = >
		array(
		0 = > ' ',
		1 = > '  ',
		2 = > ' ', ),
	2 = >
		array(
		0 = > '3',
		1 = > '4',
		2 = > '2', ),
	3 = >
		array(
		0 = > 'What Is A Hook?',
		1 = > 'HOW TO USE HOOKS ON YOUR WORDPRESS BLOG',
		2 = > 'No Hook?', ), )
	*/
	
	$toc = "";
	$tot_elem = 0;
	$lvl_index = 2; //as is found from above comment
	$title_index = 3;
	$start_lvl = 1;  //header leave h1 is already considered in caller
	$prev_lvl = $start_lvl;
	
	$anch_regex	=	"#<span.*id=['\"](.*?)['\"][^>]*>(.*?)<\/span>#s";
	
	if (!empty($matches[0]) && count($matches[0]) > 0)
	{
		//find total number of element in a single array, say matches[0];
		$tot_elem = count ($matches[0]);
		for ($i = 0; $i < $tot_elem; $i++)
		{
			$cur_lvl = $matches[$lvl_index][$i];
			
			// not considering more than h3 
			if ($cur_lvl > 3)
				continue;

				$hdr_text_match = array();
				$line = $matches[$title_index][$i];
				$anchor = ""; $hdr_text = "";
				if (preg_match($anch_regex, $line, $hdr_text_match)) 
				{
					$anchor = $hdr_text_match[1];
					$hdr_text = $hdr_text_match[2];
				}
					

			if ($prev_lvl < $cur_lvl) {
				for ($k = $prev_lvl+1; $k < $cur_lvl; $k++)
				{
					$toc .= "\n"."<ul class=\"tscom-hdr-".$k."\">"."\n".'<li class="tscom-hdr-txt-none">';
				}
				$toc .= "\n"."<ul class=\"tscom-hdr-".$cur_lvl."\">"."\n";
			} else if ($prev_lvl > $cur_lvl) {
				$lvl_diff = $prev_lvl - $cur_lvl;
				$toc .= '</li>'."\n".str_repeat('</ul>'."\n".'</li>'."\n", $lvl_diff);
			} else {
				$toc .= '</li>'."\n";
			}
			$toc .= '<li><a href="'.$article_link.'#'.$anchor.'">'.$hdr_text.'</a>';
			
			$prev_lvl = $cur_lvl;
			
		}

		$lvl_diff = $prev_lvl - $start_lvl;
		$toc .= str_repeat('</li>'."\n".'</ul>'."\n", $lvl_diff);
	}
  
	return $toc;
		
}
?>