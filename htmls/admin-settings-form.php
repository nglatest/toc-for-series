<?php
function display_toc_settings_form($gfonts, $tdoptions, $tcoptions)
{
    $toc_title_font_face = $tdoptions['toc_title_font_face'];
    $toc_title_font_style = $tdoptions['toc_title_font_style'];
    $toc_title_font_size = $tdoptions['toc_title_font_size'];
    $toc_list_font_face = $tdoptions['toc_list_font_face'];
    $toc_list_font_style = $tdoptions['toc_list_font_style'];
    $toc_list_font_size = $tdoptions['toc-list-font-size'];
    $google_fonts_api_key = $tdoptions['google_fonts_api_key'];

    $gfonts_array = array(
        'gfonts' => $gfonts,
        'saved_font' => $toc_list_font_face,
        'saved_style' => $toc_list_font_style,
        'title_saved_font' => $toc_title_font_face,
        'title_saved_style' => $toc_title_font_style,
        'title_font_color' => $tdoptions['toc_title_font_color'],
        'list_font_color' => $tdoptions['toc_list_font_color']
    );

    wp_localize_script('toc', 'gfonts_atts', $gfonts_array);

    $active_tab = 'display_options';
    if( isset( $_GET[ 'tab' ] ) ) {
        $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'display_options';
    } // end if
    ?>

    <div id="tscom-toc-wrap">
        <div class="toc-hdr">
            <div class="hdr-title">TOC Settings <i class="fa fa-arrow-right"></i></div>
            <div class="icon-settings"></div>
            <div class="clear"></div>
        </div>

        <h2 class="nav-tab-wrapper">
            <a href="?page=toc-plugin&tab=display_options" id="display" class="nav-tab <?php echo $active_tab == 'display_options' ? 'nav-tab-active' : 'nav-tab-inactive'; ?>"><i class="fa fa-columns"></i><div class="tab-lbl">Display</div></a>
            <a href="?page=toc-plugin&tab=color_options" id="colors" class="nav-tab <?php echo $active_tab == 'color_options' ? 'nav-tab-active' : 'nav-tab-inactive'; ?>"><i class="fa fa-picture-o"></i><div class="tab-lbl">Colors</div></a>
	    <a href="?page=toc-plugin&tab=help" id="colors" class="nav-tab <?php echo $active_tab == 'help' ? 'nav-tab-active' : 'nav-tab-inactive'; ?>"><i class="fa fa-life-ring"></i><div class="tab-lbl">Help</div></a>
        </h2>

         <form class="toc-form" id='toc-form' method="post" action="options.php">

            <?php if( $active_tab == 'display_options' ) {
             settings_fields('toc-plugin-settings-group');
             do_settings_sections('toc-plugin-settings-group'); ?>

            <div class="form_block">
            <label>Google Fonts API Key</label>
            <input type="text" id="toc_google_api" name="toc_options[google_fonts_api_key]" placeholder="Enter API key retrieved from Google"
                   value="<?php echo esc_attr($google_fonts_api_key); ?>"/>

            <label>Title</label><div class="small-label">(Leave it empty if do not want to display title)</div>
            <input type="text"  id="toc_title_text" name="toc_options[toc_title]" size=20 placeholder="Enter title to display at top of ToC"
                   value="<?php echo esc_attr($tdoptions['toc_title']); ?>"/>

            <h3 class="list-font-title">Select Font for Title</h3>

            <div id="title-fonts">

                <div class="block">
                    <label class="lbl-hide">Font</label>
                    <select class="toc_title_font" title="Font" name="toc_options[toc_title_font_face]" id="toc_title_font_face">';
                        <?php
                        $output = "";
                        foreach ($gfonts as $key => $val) {
                            $selected = "";
                            if ($toc_title_font_face == $key)
                                $selected = "selected";

                            $output .= '<option value="' . esc_attr($key) . '" ' . $selected . '>' . esc_html($val['name']) . '</option>';
                        }
                        echo $output;
                        ?>
                    </select>
                </div>

                <div class="block">
                    <label class="lbl-hide">Style</label>
                    <select class="toc_title_font" title="Font Style" name="toc_options[toc_title_font_style]"
                            id="toc_title_font_style"></select>
                </div>

                <div class="block">
                    <label class="lbl-hide">Size</label>
                    <select class="toc_title_font" title="Font Size" name="toc_options[toc_title_font_size]" id="toc_title_font_size">';
                        <?php
                        $output = "";
                        for ($i = 9; $i < 71; $i++) {
                            $selected = "";
                            if ($toc_title_font_size == $i)
                                $selected = "selected";
                            $output .= '<option value="' . esc_attr($i) . '" ' . $selected . '>' . esc_html($i) . '</option>';
                        }
                        echo $output;
                        ?>
                    </select>
                </div>

            </div>
		      
            <input type="hidden" class="toc_title_font_url" name="toc_options[toc_title_font_url]" id="toc_title_font_url"
                   value="<?php echo  esc_attr($tdoptions['toc_title_font_url']); ?>"/>

            <h3 class="list-font-title">Select Font for List</h3>

            <div id="list-fonts">

                <div class="block">
                    <label class="lbl-hide">Font</label>
                    <select class="toc_list_font" title="Font" name="toc_options[toc_list_font_face]" id="toc_list_font_face">';
                        <?php
                        $output = "";
                        $selected_font_key = "";
                        foreach ($gfonts as $key => $val) {
                            $selected = "";
                            if ($toc_list_font_face == $key) {
                                $selected = "selected";
                                $selected_font_key = $key;
                            }
                            $output .= '<option value="' . esc_attr($key) . '" ' . $selected . '>' . esc_html($val['name']) . '</option>';
                        }
                        echo $output;
                        ?>
                    </select>
                </div>

                <div class="block">
                    <label class="lbl-hide">Style</label>
                    <select class="toc_list_font" title="Font Style" name="toc_options[toc_list_font_style]"
                            id="toc_list_font_style"></select>
                </div>

                <div class="block">
                    <label class="lbl-hide">Size</label>
                    <select class="toc_list_font" title="Font Size" name="toc_options[toc-list-font-size]" id="toc-list-font-size">';
                        <?php
                        $output = "";
                        for ($i = 9; $i < 71; $i++) {
                            $selected = "";
                            if ($toc_list_font_size == $i)
                                $selected = "selected";
                            $output .= '<option value="' . esc_attr($i) . '" ' . $selected . '>' . esc_html($i) . '</option>';
                        }
                        echo $output;
                        ?>
                    </select>
                </div>

            </div>

            <input type="hidden" class="toc_list_font_url" name="toc_options[toc_list_font_url]" id="toc_list_font_url"
                   value="<?php echo ($tdoptions['toc_list_font_url']); ?>"/>


            <h3 class="custom-css">Custom CSS</h3>
            <textarea cols="15" name="toc_options[toc_custom_css]" placeholder="Enter CSS attributes which you want to overwrite"
                      id="toc_custom_css" rows="5"><?php echo esc_textarea($tdoptions['toc_custom_css']); ?> </textarea>

            </div>
            <div class="buttons">
                <input class="button  button-secondary" name="toc_defaults" id="toc_defaults" type="submit" value="Reset to Defaults" />
                <input style="float:right;" type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"/>
            </div>
   		      
            <?php } else if( $active_tab == 'color_options' ){
            settings_fields('toc-plugin-colors-group');
            do_settings_sections('toc-plugin-colors-group'); ?>
            <div class="form_block">
                 <div id="color_wrap">
                 <h3 class="list-font-title">Color Style for Title</h3>
                 <div class="color_block">
                     <label class="lbl">Font Color</label>
                     <input type="text" class="color-field" style="background-color: <?php echo($tcoptions['toc_title_font_color']); ?>"
                            title="Font Color" name="toc_col_options[toc_title_font_color]" id="toc_title_font_color"
                            value="<?php echo($tcoptions['toc_title_font_color']); ?>"/>

                 </div>
                 <div class="color_block">
                     <label class="lbl"> Background Color</label>
                     <input type="text" class="color-field" style="background-color: <?php echo($tcoptions['toc_title_bg_color']); ?>"
                            title="Font Color" name="toc_col_options[toc_title_bg_color]" id="toc_title_bg_color"
                            value="<?php echo($tcoptions['toc_title_bg_color']); ?>"/>

                 </div>
                 <div class="color_block">
                     <label class="lbl">Border Color</label>
                     <input type="text" class="color-field" style="background-color: <?php echo($tcoptions['toc_title_border_color']); ?>"
                            title="Font Color" name="toc_col_options[toc_title_border_color]" id="toc_title_border_color"
                            value="<?php echo($tcoptions['toc_title_border_color']); ?>"/>

                 </div>
                 <h3 class="list-font-title">Color Style for List</h3>
                 <div class="color_block">
                     <label class="lbl">Font Color</label>
                     <input type="text" class="color-field" style="background-color: <?php echo($tcoptions['toc_list_font_color']); ?>"
                                title="Font Color" name="toc_col_options[toc_list_font_color]" id="toc_list_font_color"
                                value="<?php echo($tcoptions['toc_list_font_color']); ?>"/>
                 </div>
                 <div class="color_block">
                     <label class="lbl">Background Color</label>
                     <input type="text" class="color-field" style="background-color: <?php echo($tcoptions['toc_list_bg_color']); ?>"
                            title="Font Color" name="toc_col_options[toc_list_bg_color]" id="toc_list_bg_color"
                            value="<?php echo($tcoptions['toc_list_bg_color']); ?>"/>
                 </div>

                 <div class="color_block">
                     <label class="lbl">Background  Color: hover</label>
                     <input type="text" class="color-field" style="background-color: <?php echo($tcoptions['toc_list_hover_bg_color']); ?>"
                        title="List Background  Color: hover" name="toc_col_options[toc_list_hover_bg_color]" id="toc_list_hover_bg_color"
                        value="<?php echo($tcoptions['toc_list_hover_bg_color']); ?>"/>
                 </div>
                 <div class="color_block">
                     <label class="lbl"> Text  Color: hover</label>
                     <input type="text" class="color-field" style="background-color: <?php echo($tcoptions['toc_list_hover_fg_color']); ?>"
                            title="List Foreground  Color: hover" name="toc_col_options[toc_list_hover_fg_color]" id="toc_list_hover_fg_color"
                            value="<?php echo($tcoptions['toc_list_hover_fg_color']); ?>"/>
                 </div>
                 <h3 class="list-font-title">Current Post Colors</h3>
                 <div class="color_block">
                     <label class="lbl">Text Color</label>
                     <input type="text" class="color-field" style="background-color: <?php echo($tcoptions['toc_list_cur_post_fg_color']); ?>"
                            title="List Foreground  Color: hover" name="toc_col_options[toc_list_cur_post_fg_color]" id="toc_list_cur_post_fg_color"
                            value="<?php echo($tcoptions['toc_list_cur_post_fg_color']); ?>"/>
                 </div>   <div class="color_block">
                     <label class="lbl">Background Color</label>
                     <input type="text" class="color-field" style="background-color: <?php echo($tcoptions['toc_list_cur_post_bg_color']); ?>"
                            title="List Foreground  Color: hover" name="toc_col_options[toc_list_cur_post_bg_color]" id="toc_list_cur_post_bg_color"
                            value="<?php echo($tcoptions['toc_list_cur_post_bg_color']); ?>"/>
                 </div>
             </div>
	     </div>
            <div class="buttons">
                <input class="button  button-secondary" name="toc_defaults" id="toc_defaults" type="submit" value="Reset to Defaults" />
                <input style="float:right;" type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"/>
            </div>

	    <?php } else if( $active_tab == 'help' ){?>
             <div class="help_block">
                 <div class="accordion">
                     <div class="toc-accordion-section">
                         <div class="accordion-section-title active" href="#accordion-1">Plugin Settings</div>
                         <div id="accordion-1" class="accordion-section-content">
                            <div class="help_title">Getting Started</div>
                             <p>On fresh installation, most of the fields must be blank. To get started, there are two
                                 ways to use that. Either to fill each field manually as per your choice and then get
                                 started up or second way is to use default values. To use default values just click
                                 on 'Reset to Defaults' button at the bottom and then press 'Save Changes' button
                                 to save them.</p>
                             <p><strong>Note: </strong><em>Reset to Defaults</em> and <em>Save Changes</em> button works
                                 for each tab individually. It means if <em>reset</em> button is pressed on 'Display'
                                 tab then it will reset the settings for display tab only. To reset colors, you need to
                                 go to that respective tab and then press the required button.</p>
                             <p class="highlight">Also, it is necessary to press 'Save Changes' button after you do
                                 <em>reset</em>. Otherwise change will not take effect.</p>
                             <div class="help_title">Display Sections overview</div>
                             <p><strong>1) Google Fonts Api key: </strong>TSCom ToC is using vast collection of google fonts. To
                             use them in your plugin, you need to register for google API key. Refer
                                 <a href=""https://developers.google.com/api-client-library/python/guide/aaa_apikeys">
                                 Google's developer page</a> on how to get that. </p>
                             <p><strong>2) Title: </strong>Enter the string which you want to show at the top of the tree.</p>
                             <p><strong>3) Fonts: </strong>This section is further divided into two sections where you
                                 can select font, type and size of the text to display. </p>
                         </div><!--end .accordion-section-content-->
                     </div><!--end .accordion-section-->

                     <div class="toc-accordion-section">
                         <div class="accordion-section-title" href="#accordion-2">Color Settings</div>
                         <div id="accordion-2" class="accordion-section-content">
                             <div class="help_title">Color Sections overview</div>
                             <p>This section is used to select color for various areas and texts in the ToC. Even if
                                 each name is self explanatory but we are also providing the snapshot of the tree where you
                                 can relate various areas</p>
                             <div class="help_title">Tree Layout and Styles</div>
                             <div class="tree_layout"></div>
                             </div><!--end .accordion-section-content-->
                     </div><!--end .accordion-section-->

                     <div class="toc-accordion-section">
                         <div class="accordion-section-title" href="#accordion-3">Post Settings</div>
                         <div id="accordion-3" class="accordion-section-content">
                             <p>We also need to prepare our posts to display the tree. For this, there are two things we
                                 need to achieve first. First one is meta settings, discussed here. </p>

                             <div class="help_title">Meta Settings</div>

                             <p>When you go to edit your post or add new post, you should notice a new box displayed in
                                 right side of your screen. Here you need to provide name of the series to which you
                                 want to add your post. If you want to start a new series then select <em>other</em>
                                 from <em>Series</em> and then enter name under <em>New Series</em></p>

                             <div class="meta_settings"></div>
                             <p><strong>Unique anchor: </strong> Select it if you want to make sure that anchor-ids
                                 generated for your headers(h1/h2/h3) should always be unique.</p>
                        </div>
                         <div class="toc-accordion-section">
                             <div class="accordion-section-title" href="#accordion-4">Shortcode Settings</div>
                             <div id="accordion-4" class="accordion-section-content">
                                 <p>You can insert shortcode either in your post or in any of your widget with the help
                                     of <em>Text Widget</em>.</p>
                                  <div class="help_title">Insert Shortcode</div>
                                 <p>By default tree will not be shown in your article even if it is part of any series.
                                     If you want to display it then insert shortcode to do that:</p>
                                 <p><code>[tscom-toc hdr_depth="h2" series="test1" ]My Series[/tscom-toc]</code></p>
                                 <p><em>tscom-toc</em> is the name of the shortcode. </p>
                                 <p><em>hdr_depth</em> is used to select the level of the headers to display in the
                                     tree. Level till h3 is supported. Default level is also h3.</p>
                                 <p><em>series</em> is the name of the series which you want to display. Default is the
                                     name of current article's series.</p>
                                 <p><em>My Series</em> represents the title of the series to display at the top. This
                                 could be replaced with any string which you want to display. If this is not mentioned
                                 here then it will try to fetch the value from your plugin settings mention at top.</p>
                                 <div class="help_title">TinyMCE button for Shortcode</div>
                                 <p>A button has been provided in TinyMCE editor as highlighted in below image.</p>
                                 <div class="tinymce_btn"></div>
                                 <p>When you press this button it will prompt you for header level and series name as
                                 mentioned above.</p>
                             </div>
                         <!--end .accordion-section-content-->
                     </div><!--end .accordion-section-->
                 </div><!--end .accordion-->
            <?php } // end if/else
            ?>
            </div>
            <span class="success"><?php echo $successMessage;?></span>
        </form>
    </div>
<?php
}
?>
