=== Plugin Name ===
Contributors: techsyard
Donate link: http://techsmashing.com/
Tags: comments, spam
Requires at least: 3.9
Tested up to: 4.1
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Displays all related articles and their respective headers as a tree inside the contents

== Description ==

Most of the time we are writing many articles which are related to each other. So sometimes it is required to display
all those articles and their respective headers in a 'Table of Contents' format. This is where TSCom's ToC plugin could
help us.

== Installation ==

1. Upload `toc-for-series` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go to 'TSCom ToC' plugin settings to set various options. You can also refer 'Help' section available there.

== Screenshots ==

1. tscom_toc_view1.png
2. tscom_toc_view2.png
3. tscome_toc_tinymce_btn.png
4. tscom_meta_settings.png
5. setting_display.png
6. setting-colors.png

== Changelog ==

= 1.0 =
* First version

